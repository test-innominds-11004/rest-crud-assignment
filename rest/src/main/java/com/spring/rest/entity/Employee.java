package com.spring.rest.entity;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Employee {
	



	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long Employee_id;
	private String First_Name;
	private String Last_Name;
	private String Email;
	private Integer Phone_Number;
	private Long Job_id;
	private int Salary;
	private Long Manager_id;
	
	
	
	

	
	
	
	
	public Long getEmployee_id() {
		return Employee_id;
	}

	public void setEmployee_id(Long Employee_id) {
		this.Employee_id =Employee_id;
	}

	public String getFirst_Name() {
		return First_Name;
	}

	public void setFirst_Name(String First_Name) {
		this.First_Name = First_Name;
	}
	public String getLast_Name() {
		return Last_Name;
	}

	public void setLast_Name(String Last_Name) {
		this.Last_Name = Last_Name;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String Email) {
		this.Email = Email;
	}

	public Integer getPhone_Number() {
		return Phone_Number;
	}

	public void setPhone_Number(Integer Phone_Number) {
		this.Phone_Number = Phone_Number;
	}

	public Long getJob_id() {
		return Job_id;
	}
	public void setJob_id(Long Job_id) {
		this.Job_id =Job_id;
	}
	
	public Integer getSalary() {
		return Salary;
	}
	public void setSalary(Integer Salary) {
		this.Salary = Salary;
	}
	public Long getManager_id() {
		return Manager_id;
	}

	public void setManager_id(Long Manager_id) {
		this.Manager_id =Manager_id;
	}



	

	public Employee(Long Employee_id, String First_Name, String Last_Name,String Email ,Integer Phone_Number, Long Job_id,Integer Salary,Long Manager_id) {
		super();
		this.Employee_id = Employee_id;
		this.First_Name = First_Name;
		this.Last_Name = Last_Name;
		this.Email = Email;
		this.Phone_Number= Phone_Number;
		this.Job_id = Job_id;
		this.Salary = Salary;
		this.Manager_id = Manager_id;
		
	}

	public Employee() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
